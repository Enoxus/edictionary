//
//  WordInfoController.swift
//  eDictionary
//
//  Created by Enoxus on 02/10/2018.
//  Copyright © 2018 Enoxus. All rights reserved.
//

import Foundation
import UIKit

class WordInfoController : UIViewController {
    @IBOutlet weak var wordField: UITextField!
    @IBOutlet weak var transField: UITextField!
    @IBOutlet weak var contextField: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        contextField.layer.borderWidth = CGFloat(Float(0.8))
        contextField.layer.borderColor = UIColor(red: 5/255, green: 5/255, blue: 5/255, alpha: 0.15).cgColor
        contextField.layer.cornerRadius = CGFloat(Float(5.0))
        
        wordField.text = list[myIndex]
        transField.text = translations[myIndex]
        contextField.text = contexts[myIndex]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}

//
//  WordAddDialog.swift
//  eDictionary
//
//  Created by Enoxus on 01/10/2018.
//  Copyright © 2018 Enoxus. All rights reserved.
//

import Foundation
import UIKit

class WordAddDialog : UIViewController {
    
    @IBOutlet weak var wordField: UITextField!
    @IBOutlet weak var transField: UITextField!
    @IBOutlet weak var contextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if wordField.text != "" && transField.text != "" {
            list.append(wordField.text ?? "error")
            translations.append(transField.text ?? "error")
            contexts.append(contextField.text ?? "error")
            
            UserDefaults.standard.set(list, forKey: "words")
            UserDefaults.standard.set(translations, forKey: "translations")
            UserDefaults.standard.set(contexts, forKey: "contexts")
            performSegue(withIdentifier: "backSegue", sender: self)
        }
    }
}

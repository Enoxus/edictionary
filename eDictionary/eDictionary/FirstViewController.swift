//
//  FirstViewController.swift
//  eDictionary
//
//  Created by Enoxus on 27/09/2018.
//  Copyright © 2018 Enoxus. All rights reserved.
//

import UIKit
var list = [String]()
var translations = [String]()
var contexts = [String]()
var myIndex = 0

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var myTableView: UITableView!
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        cell.backgroundColor = UIColor(red: 18/100, green: 18/100, blue: 18/100, alpha: 1)
        cell.textLabel?.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 14/100, green: 14/100, blue: 14/100, alpha: 1)
        cell.selectedBackgroundView = backgroundView
        cell.textLabel?.text = list[indexPath.row]
        cell.detailTextLabel?.text = translations[indexPath.row]
        cell.detailTextLabel?.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.75)
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            list.remove(at: indexPath.row)
            translations.remove(at: indexPath.row)
            contexts.remove(at: indexPath.row)
            UserDefaults.standard.set(list, forKey: "words")
            UserDefaults.standard.set(translations, forKey: "translations")
            UserDefaults.standard.set(contexts, forKey: "contexts")
            myTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myIndex = indexPath.row
        performSegue(withIdentifier: "segue", sender: self)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        list = UserDefaults.standard.object(forKey: "words") as? [String] ?? [String]()
        translations = UserDefaults.standard.object(forKey: "translations") as? [String] ?? [String]()
        contexts = UserDefaults.standard.object(forKey: "contexts") as? [String] ?? [String]()
        
        myTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}

